package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	
	import ru.nngames.bingo.gamebox.Theme.MainTheme;
	import ru.nngames.bingo.gamebox.Theme.ThemeBase;
	import ru.nngames.bingo.gamebox.ui.root.view.ViewStack;
	
	import starling.core.Starling;
	import starling.events.Event;
	import starling.utils.RectangleUtil;
	import starling.utils.ScaleMode;
	import starling.utils.SystemUtil;
	
	import theme.MainTheme;
	import theme.ThemeBase;
	
	import ui.root.ViewStack;
	
	[SWF(width="1920",height="1080",frameRate="60",backgroundColor="#000000", wmode='direct')]
	public class SampleFeathersMenu extends Sprite
	{
		private var _starling:Starling;
		
		private const StageWidth:int  = 320;
		private const StageHeight:int = 480;
				
		public function SampleFeathersMenu()
		{
			super();
			
			// support autoOrients
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			var iOS:Boolean = SystemUtil.platform == "IOS";
			var stageSize:Rectangle  = new Rectangle(0, 0, StageWidth, StageHeight);
			var screenSize:Rectangle = new Rectangle(0, 0, stage.fullScreenWidth, stage.fullScreenHeight);
			var viewPort:Rectangle = RectangleUtil.fit(screenSize, screenSize, ScaleMode.SHOW_ALL, iOS);
			var scaleFactor:int = viewPort.width < 480 ? 1 : 2; // midway between 320 and 640
			
			Starling.multitouchEnabled = true;
			
			stage.stageFocusRect = false;
			
			initThemeBase();
			
			_starling = new Starling(ViewStack, stage, new Rectangle( ThemeBase.GAME_X,ThemeBase.GAME_Y, ThemeBase.SCREEN_WIDTH, ThemeBase.ORIGINAL_HEIGHT));
			
			_starling.enableErrorChecking = Capabilities.isDebugger;
			_starling.skipUnchangedFrames = true;
			_starling.simulateMultitouch  = false;
			_starling.showStats = false;
			
			_starling.addEventListener(Event.CONTEXT3D_CREATE, contextHandler);
			
			_starling.start();
		}
		
		private function contextHandler(e:Event):void
		{
			Starling.current.removeEventListener( Event.CONTEXT3D_CREATE, contextHandler );
			// if you need to init other framework like PureMVC or Robotlegs
		}
		
		private function initThemeBase():void{
			ThemeBase.SCREEN_WIDTH=stage.fullScreenWidth;
			ThemeBase.SCREEN_HEIGHT=stage.fullScreenHeight;
			
			var stKoef:Number = ThemeBase.SCREEN_WIDTH/ThemeBase.SCREEN_HEIGHT;
			var appKoef:Number = ThemeBase.ORIGINAL_WIDTH/ThemeBase.ORIGINAL_HEIGHT;
			if (stKoef > appKoef) {
				ThemeBase.GAME_HEIGHT=ThemeBase.SCREEN_HEIGHT;
				ThemeBase.GAME_WIDTH = ThemeBase.GAME_HEIGHT * appKoef;
			} else {
				ThemeBase.GAME_WIDTH = ThemeBase.SCREEN_WIDTH;
				ThemeBase.GAME_HEIGHT = ThemeBase.GAME_WIDTH / appKoef;
			}
			ThemeBase.GAME_X = (ThemeBase.SCREEN_WIDTH - ThemeBase.GAME_WIDTH) / 2;
			ThemeBase.GAME_Y = (ThemeBase.SCREEN_HEIGHT - ThemeBase.GAME_HEIGHT) / 2;
			
			// scale factors of resized content to real stage dimension
			ThemeBase.SCALE = ThemeBase.GAME_WIDTH/ThemeBase.ORIGINAL_WIDTH;
		}
	}
}