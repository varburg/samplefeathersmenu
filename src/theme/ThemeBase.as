package theme
{
	import feathers.themes.StyleNameFunctionTheme;
	
	import starling.utils.AssetManager;
	
	public class ThemeBase extends StyleNameFunctionTheme
	{
		public static const ORIGINAL_WIDTH:int=1920;
		public static const ORIGINAL_HEIGHT:int=1080;
		public static var SCREEN_WIDTH:int;
		public static var SCREEN_HEIGHT:int;
		public static var SCALE:Number;
		public static var GAME_X:Number;
		public static var GAME_Y:Number;
		public static var GAME_WIDTH:Number;
		public static var GAME_HEIGHT:Number;
		public static var startAsset:AssetManager=new AssetManager(1,true);
		
		public function ThemeBase()
		{
			super();
		}
	}
}