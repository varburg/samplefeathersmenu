package theme
{
	import flash.desktop.Icon;
	import flash.text.TextFormat;
	
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.List;
	import feathers.controls.ScrollContainer;
	import feathers.controls.ToggleButton;
	import feathers.controls.text.TextFieldTextRenderer;
	import feathers.core.ITextRenderer;
	
	import flashx.textLayout.formats.TextAlign;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.textures.TextureAtlas;
	
	public class MainTheme extends ThemeBase
	{
		public static const LIST_MENU_STYLE:String='LIST_MENU_STYLE';
		public static const LAYOUTGROUP_MENU_ITEM_STYLE:String='LAYOUTGROUP_MENU_ITEM_STYLE';
		public static const LAYOUTGROUP_MENU_GROUP_STYLE:String='LAYOUTGROUP_MENU_GROUP_STYLE';
		
		public static const SWITCH1_BUTTON_STYLE:String='SWITCH1_BUTTON_STYLE';
		public static const LEFT_ARROW_BUTTON_STYLE:String='LEFT_ARROW_BUTTON_STYLE';
		public static const RIGHT_ARROW_BUTTON_STYLE:String='RIGHT_ARROW_BUTTON_STYLE';
		
		public static const TEXT_ITEM:String='TEXT_ITEM';
		public static const TEXT_FOOTER:String='TEXT_FOOTER';
		
		
		[Embed(source="/assets/fonts/LATO-BOLD_0.TTF", fontWeight="Bold", fontName="LATOBOLD", mimeType="application/x-font", embedAsCFF="false")]
		public static const LATOBOLD:Class;
		
		public static const FONT_LATOBOLD_NAME:String = "LATOBOLD";
		
		private var menuItemLabel:TextFormat;
		private var footerItemLabel:TextFormat;
		private var itemColor:Number = 0x45475c;
		private var bgColor:Number = 0x383a4c;
		private var groupBgColor:Number = 0x4c4e5f;
				
		public function MainTheme()
		{
			super();
			initialize()
		}
		
		private function initialize():void
		{
			initializeFonts();
			initializeStyleProviders();
		}
		
		protected function initializeFonts():void
		{
			menuItemLabel = new TextFormat( FONT_LATOBOLD_NAME, 40, 0xffffff, null, null, null, null, null, "left" );
			footerItemLabel = new TextFormat( FONT_LATOBOLD_NAME, 40, itemColor, null, null, null, null, null, "left" );
		}
		
		protected function initializeStyleProviders():void
		{
			getStyleProviderForClass(ScrollContainer).setFunctionForStyleName(LIST_MENU_STYLE, setLayoutMenu);
			getStyleProviderForClass(LayoutGroup).setFunctionForStyleName(LIST_MENU_STYLE, setLayoutMenu);
			getStyleProviderForClass(LayoutGroup).setFunctionForStyleName(LAYOUTGROUP_MENU_ITEM_STYLE, setLayoutMenuItem);
			getStyleProviderForClass(LayoutGroup).setFunctionForStyleName(LAYOUTGROUP_MENU_GROUP_STYLE, setLayoutMenuGroup);

			getStyleProviderForClass(ToggleButton).setFunctionForStyleName(SWITCH1_BUTTON_STYLE, setSwitch1Button);
			getStyleProviderForClass(ToggleButton).setFunctionForStyleName(LEFT_ARROW_BUTTON_STYLE, setLeftArrowButton);
			getStyleProviderForClass(ToggleButton).setFunctionForStyleName(RIGHT_ARROW_BUTTON_STYLE, setRightArrowButton);

			getStyleProviderForClass(Label).setFunctionForStyleName(TEXT_ITEM, setTextForItem);
			getStyleProviderForClass(Label).setFunctionForStyleName(TEXT_FOOTER, setTextForFooter);
		}
		
		private function setLayoutMenu(group:*):void
		{
			var bgSkin:Quad = new Quad(30, 30, bgColor);
			group.backgroundSkin = bgSkin;
		}
		
		private function setLayoutMenuGroup(group:*):void
		{
			var bgSkin:Quad = new Quad(30, 30, groupBgColor);
			group.backgroundSkin = bgSkin;
		}
		
		private function setLayoutMenuItem(group:*):void
		{
			var bgSkin:Quad = new Quad(30, 30, itemColor);
			group.backgroundSkin = bgSkin;
		}
		
		private function setTextForItem(label:Label):void
		{
			label.textRendererFactory = function():ITextRenderer{
				var textRenderer:TextFieldTextRenderer = new TextFieldTextRenderer();
				
				textRenderer.textFormat = menuItemLabel;
				textRenderer.embedFonts = true;
				textRenderer.isHTML = false;
				textRenderer.wordWrap = false;
				
				return textRenderer;
			}
			
		}
		
		private function setTextForFooter(label:Label):void
		{
			label.textRendererFactory = function():ITextRenderer{
				var textRenderer:TextFieldTextRenderer = new TextFieldTextRenderer();
				
				textRenderer.textFormat = footerItemLabel;
				textRenderer.embedFonts = true;
				textRenderer.isHTML = false;
				textRenderer.wordWrap = false;
				
				return textRenderer;
			}
			
		}
		
		private function setSwitch1Button(btn:ToggleButton):void
		{
			var onImage:Image = new Image(ThemeBase.startAsset.getTexture("sw1On"));
			var offImage:Image = new Image(ThemeBase.startAsset.getTexture("sw1Off"));
			btn.defaultSelectedSkin = onImage;
			btn.defaultSkin = offImage;
		}
		
		private function setLeftArrowButton(btn:ToggleButton):void
		{
			var icon:Image = new Image(ThemeBase.startAsset.getTexture("leftBtnArr"));
			btn.defaultIcon = btn.defaultSelectedIcon = icon;
			btn.labelFactory = function():ITextRenderer{
				var textRenderer:TextFieldTextRenderer = new TextFieldTextRenderer();
				
				textRenderer.textFormat = menuItemLabel;
				textRenderer.embedFonts = true;
				textRenderer.isHTML = false;
				textRenderer.wordWrap = false;
				
				return textRenderer;
			}
			btn.iconPosition = TextAlign.LEFT;
		}
		
		private function setRightArrowButton(btn:ToggleButton):void
		{
			var icon:Image = new Image(ThemeBase.startAsset.getTexture("rightBtnArr"));
			btn.defaultIcon = btn.defaultSelectedIcon = icon;
			btn.labelFactory = function():ITextRenderer{
				var textRenderer:TextFieldTextRenderer = new TextFieldTextRenderer();
				
				textRenderer.textFormat = menuItemLabel;
				textRenderer.embedFonts = true;
				textRenderer.isHTML = false;
				textRenderer.wordWrap = false;
				
				return textRenderer;
			}
			btn.iconPosition = TextAlign.RIGHT;
		}
	}
}