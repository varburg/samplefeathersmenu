package ui.root
{
	import flash.filesystem.File;
	
	import feathers.controls.LayoutGroup;
	
	import ru.nngames.bingo.gamebox.Theme.ThemeBase;
	import ru.nngames.bingo.gamebox.event.MVCEvent;
	
	import theme.MainTheme;
	import theme.ThemeBase;
	
	import ui.components.Menu;
	
	public class ViewStack extends LayoutGroup
	{
		public function ViewStack()
		{
			super();
			this.scale=ThemeBase.SCALE;
		}
		
		override protected function initialize():void
		{
			super.initialize();
			new MainTheme();
			var appDir:File = File.applicationDirectory;
			ThemeBase.startAsset.enqueue(appDir.resolvePath("assets"));			
			ThemeBase.startAsset.loadQueue(function(ratio:Number):void
			{
				if (ratio == 1) {
					startApp();
				}
			});
		}
		
		private function startApp():void
		{
			var menu:Menu = new Menu();
			addChild(menu);
			menu.validate();
		}
	}
}