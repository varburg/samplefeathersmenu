package ui.components
{
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.ToggleButton;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalAlign;
	
	import starling.events.Event;
	
	import theme.MainTheme;
	
	public class MenuItem extends LayoutGroup
	{
		private var data:Object;
		private var ll:HorizontalLayout;
		private var label:Label;
		private var sw2btn:ToggleButton;
		
		public function MenuItem(data:Object)
		{
			super();
			this.data = data;
		}
		
		override protected function initialize():void
		{
			super.initialize();
			if (data.type == MenuItemTypes.FOOTER) {
				styleName = MainTheme.LIST_MENU_STYLE;
			} else {
				styleName = MainTheme.LAYOUTGROUP_MENU_ITEM_STYLE;
			}
			ll = new HorizontalLayout();
			ll.hasVariableItemDimensions = true;
			
			ll.paddingLeft = 50;
			layout = ll;
			ll.verticalAlign = VerticalAlign.MIDDLE;
			
			label = new Label();
			label.styleName = MainTheme.TEXT_ITEM;
			label.text = data.name;
			label.validate();
			//label.x = 50;
			//label.y = (height-label.height)*0.5;
			switch (data.type) {
				case MenuItemTypes.HEADER:
					var backBtn:ToggleButton = new ToggleButton();
					backBtn.label = " BACK";
					backBtn.validate();
					ll.gap = (width-label.width)/2-ll.paddingLeft-backBtn.width-label.width/2;
					backBtn.styleName = MainTheme.LEFT_ARROW_BUTTON_STYLE;
					break;
				case MenuItemTypes.FOOTER:
					ll.horizontalAlign = HorizontalAlign.CENTER;
					label.styleName = MainTheme.TEXT_FOOTER;
					break;
				case MenuItemTypes.SWITCH1:
					ll.paddingRight = ll.paddingLeft;
					var sw1btn:ToggleButton = new ToggleButton();
					sw1btn.styleName = MainTheme.SWITCH1_BUTTON_STYLE;
					sw1btn.validate();
					ll.gap = width-2*ll.paddingLeft-label.width-sw1btn.width;
					break;
				case MenuItemTypes.SWITCH2:
					sw2btn = new ToggleButton();
					sw2btn.addEventListener(Event.TRIGGERED, onSw2BtnTriggered);
					sw2btn.isSelected = false;
					sw2btn.styleName = MainTheme.RIGHT_ARROW_BUTTON_STYLE;
					sw2btn.label = "OFF ";
					sw2btn.validate();
					ll.gap = width-2*ll.paddingLeft-label.width-sw2btn.width;
					break;
				case MenuItemTypes.SUBMENU:
					sw2btn = new ToggleButton();
					sw2btn.label = "   ";
					sw2btn.styleName = MainTheme.RIGHT_ARROW_BUTTON_STYLE;
					sw2btn.validate();
					ll.gap = width-2*ll.paddingLeft-label.width-sw2btn.width;
					break;
			}
			
			if (backBtn) {
				addChild(backBtn);
			}			
			addChild(label);
			if (sw1btn) {
				addChild(sw1btn);
			}
			if (sw2btn) {
				addChild(sw2btn);
			}
		}
		
		private function onSw2BtnTriggered(e:Event):void
		{
			
			var btn:ToggleButton = e.target as ToggleButton;
			setSw2Btn(btn);
			btn.validate();
			ll.gap = width-2*ll.paddingLeft-label.width-btn.width;
		}
		
		private function setSw2Btn(btn:ToggleButton):void
		{
			trace(btn.isSelected);
			if (btn.isSelected) {
				btn.label = "OFF ";
			} else {
				btn.label = " ON ";
			}
		}
	}
}