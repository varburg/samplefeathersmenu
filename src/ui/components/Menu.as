package ui.components
{
	import feathers.controls.LayoutGroup;
	import feathers.controls.List;
	import feathers.controls.ScrollContainer;
	import feathers.controls.renderers.LayoutGroupListItemRenderer;
	import feathers.layout.AnchorLayout;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	
	import starling.display.Quad;
	
	import theme.MainTheme;
	import theme.ThemeBase;
	
	public class Menu extends ScrollContainer
	{
		private var itemHeight:Number = 128;
		private var data:Array = new Array();		
		
		public function Menu()
		{
			super();
			data.push([{name:"OPTIONS", type:"header"}]);
			data.push([{name:"Sounds", type:"switch1"},{name:"Ghost image", type:"switch1"},{name:"Visible border", type:"switch1"},{name:"Larger pieces", type:"switch1"}]);
			data.push([{name:"Tray", type:"switch2"},{name:"Background", type:"submenu"}]);
			data.push([{name:"Restore Previous Purchases", type:"submenu"},{name:"Daily Puzzle Reminder", type:"switch2"},{name:"v1.1.0(59)", type:"footer"}]);
		}
		
		override protected function initialize():void
		{
			super.initialize();
			styleName = MainTheme.LIST_MENU_STYLE;
			var ll:VerticalLayout=new VerticalLayout();
			
			ll.gap = itemHeight;
			this.layout = ll;
			width = 2*ThemeBase.ORIGINAL_WIDTH/3;
			height = ThemeBase.ORIGINAL_HEIGHT;
			addHeader();
		}
		
		private function addHeader():void
		{
			for (var i:int=0;i<data.length;i++) {
				var ig:MenuItemGroup = new MenuItemGroup(data[i], width);
				ig.width = width;
				ig.height = data[i].length*itemHeight+2;
				ig.validate();
				addChild(ig);
			}
		}
	}
}