package ui.components
{
	public class MenuItemTypes
	{
		public static const HEADER:String = "header";
		public static const SWITCH1:String = "switch1";
		public static const SWITCH2:String = "switch2";
		public static const SUBMENU:String = "submenu";
		public static const FOOTER:String = "footer";
		
		
		public function MenuItemTypes()
		{
		}
	}
}