package ui.components
{
	import feathers.controls.LayoutGroup;
	import feathers.layout.VerticalLayout;
	
	import ru.nngames.bingo.gamebox.Theme.ThemeBase;
	
	import starling.textures.TextureAtlas;
	
	import theme.MainTheme;
	import theme.ThemeBase;
	
	public class MenuItemGroup extends LayoutGroup
	{
		private var data:Array;
		private var ww:Number;
		
		public function MenuItemGroup(data:Array, ww:Number)
		{
			super();
			this.data = data;
			this.ww = ww;
		}
		
		override protected function initialize():void
		{
			super.initialize();
			styleName = MainTheme.LAYOUTGROUP_MENU_GROUP_STYLE;
			var ll:VerticalLayout = new VerticalLayout();
			ll.gap=ll.paddingTop=ll.paddingBottom=ll.paddingLeft=ll.paddingRight = 2;
			layout = ll;
			for (var i:int=0;i<data.length;i++) {
				var item:MenuItem = new MenuItem(data[i]);
				item.width = ww-4;
				item.height = 126;
				addChild(item);
			}
		}
	}
}